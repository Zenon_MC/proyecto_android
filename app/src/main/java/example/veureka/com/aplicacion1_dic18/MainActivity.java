package example.veureka.com.aplicacion1_dic18;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Se agrega un objeto de la clase RecyclerView
        RecyclerView vista = findViewById(R.id.rvDummyCards);
        vista.setHasFixedSize(true);
        vista.setLayoutManager(new LinearLayoutManager(this));
        vista.setAdapter(new DummyCardAdapter(50));

    }
    //Implementación de la barra de Acciones
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        switch (id){
            case R.id.accion_config:
                return true;
            case R.id.accion_login:
                Intent editIntent = new Intent(MainActivity.this, LoginActivity.class);
                MainActivity.this.startActivity(editIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                return true;
            case R.id.accion_navegar:
                Intent callIntent = new Intent(MainActivity.this, ActivityNavigation.class);
                MainActivity.this.startActivity(callIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                return true;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.accion_config) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    ////===============////
    public void iniciarActividad(View view) {
        Intent actividad2 = new Intent(MainActivity.this, LoginActivity.class);
        MainActivity.this.startActivity(actividad2);
    }
}
