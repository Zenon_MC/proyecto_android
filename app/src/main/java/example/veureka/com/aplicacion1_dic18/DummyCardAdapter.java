package example.veureka.com.aplicacion1_dic18;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by veureka on 18/12/17.
 */

public class DummyCardAdapter
        extends RecyclerView.Adapter<DummyCardAdapter.CardViewHolder>{
    private int numero_Tarjetas;

    public DummyCardAdapter(int numero_Tarjetas){
        this.numero_Tarjetas = numero_Tarjetas;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        CardViewHolder vh = new CardViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int i) {
        holder.setText(i);
    }

    @Override
    public int getItemCount() {
        return numero_Tarjetas;
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        public TextView idTarjeta;
        public ImageView imagen;

        public CardViewHolder(LinearLayout itemView) {
            super(itemView);
            idTarjeta = itemView.findViewById(R.id.tvCount);
        }

        public void setText(int num){
            idTarjeta.setText("Esta es la tarjeta: " + num);

        }
    }
}
